@extends('layouts.main')

@section('judul')
<h1 class="text-center" style="color: aliceblue">Detail Buku</h1>
@endsection

@section('content')
<section>
    <img src="{{asset('/img/'. $buku->thumbnail)}}" alt="" class="mx-auto d-block">
    <h1 class="mt-3">{{$buku->judulbuku}}</h1>
    <ul class="navigasi">
        <li>
            <a href="/genre/{{$buku->genre_id}}">
                <span class="badge badge-info">{{$buku->genre->nama}}</span>
            </a>
            <small class="ml-3">added {{$buku->created_at->diffForHumans()}}</small>
        </li>
        <li>
            @auth
            <form action="/buku/{{$buku->id}}" method="post">
                @csrf
                @method('delete')
                <a href="/buku/{{$buku->id}}/edit" class="text-light">
                  <span class="btn btn-sm" style="background-color: #de9f0d;">
                    <i class="mdi mdi-pencil">
                        Edit
                    </i>
                </span>
                </a>
                <a class="text-light" >
                    <span class="btn btn-danger btn-sm" >
                        <i class="mdi mdi-delete">
                            <input type="submit" value="delete" class="bg-danger text-light" style="border: none">
                        </i>
                    </span>
                </a>
            </form>
            @endauth
        </li>
    </ul>
    
    <p class="mt-2">{{$buku->sinopsis}}</p>
</section>

<section class="mt-4">
    <div class="row">
        <div class="col-2">
            <h6>Penulis</h6>
            <h6>Penerbit</h6>
            <h6>Tahun Terbit</h6>
            <h6>Jumlah Halaman</h6>
        </div>
        <div class="col-10">
            <h6>: {{$buku->penulis}}</h6>
            <h6>: {{$buku->penerbit}}</h6>
            <h6>: {{$buku->tahun}}</h6>
            <h6>: {{$buku->halaman}}</h6>
        </div>
    </div>
</section>
<hr class="bg-light">


<section>
    <div class="row">
        <div class="col-6">
            <h2 class="mb-5">komentar</h2>
            @forelse ($buku->komentar as $item)
                <h5><b>{{$item->user->name}}</b></h5>
                <p>{{$item->isi}}</p>
                <br>
            @empty
                <h5>Belum ada komentar !</h5>
            @endforelse
        </div>
        <div class="col-6">
            @auth
            <form action="/komentar" class="ml-2" method="POST">
                @csrf
                <div class="form-group">
                    <input type="hidden" name="buku_id" value="{{$buku->id}}">
                    <textarea  class="form-control text-light" rows="5" name="isi" placeholder="Tulis Komentar Anda Disini!"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            @endauth
        </div>
    </div>
</section>
@endsection