@extends('layouts.main')

@section('judul')
<h1 class="text-center" style="color: aliceblue">Tambah Buku</h1>
@endsection
    
@section('content')
<form action="/" method="POST" enctype="multipart/form-data">
    @csrf
    
    <div class="form-group">
      <label>Judul Buku</label>
      <input type="text" class="form-control text-light" name="judulbuku">
    </div>
    @error('judulbuku')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label>Sinopsis</label>
      <textarea  class="form-control text-light" style="font-size: 1em" rows="10" name="sinopsis"></textarea>
    </div>
    @error('sinopsis')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label>Penulis</label>
      <input type="text" class="form-control text-light" name="penulis">
    </div>
    @error('penulis')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label>Penerbit</label>
      <input type="text" class="form-control text-light" name="penerbit">
    </div>
    @error('penerbit')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Tahun Terbit</label>
      <input type="number" class="form-control text-light" name="tahun">
    </div>
    @error('tahun')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label>Jumlah Halaman</label>
      <input type="number" class="form-control text-light" name="halaman">
    </div>
    @error('halaman')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
        <label>Genre</label>
        <select name="genre_id" class="js-example-basic-single " style="width: 100%">
          <option value="">----Pilih Genre----</option>
          @foreach ($genre as $item)
            <option value="{{$item->id}}">{{$item->nama}}</option>
          @endforeach
        </select>
    </div>
    @error('genre_id')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label>Thumbnail</label>
      <input type="file" class="form-control text-light" name="thumbnail">
    </div>
    @error('thumbnail')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection