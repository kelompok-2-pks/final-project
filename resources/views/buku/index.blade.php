@extends('layouts.main')

@section('judul')
<h1 class="text-center" style="color: aliceblue">Daftar Buku</h1>
@endsection
    
@section('content')

<div class="container-fluid">
  <div class="row">
    @forelse ($buku as $item)
      <div class="col-md-4">
        <a href="/buku/{{$item->id}}">
            <div class="card text-light kartu mt-4" style="height: 450px;" >
                <img class="card-img-top" style="height: 250px;" src="{{asset('img/'. $item->thumbnail)}}" alt="Card image cap">
                <div class="card-body  pb-0">
                    <h3>{{$item->judulbuku}}</h3>
                    <p class="card-text link">{{Str::limit($item->sinopsis, 60, $end='...')}}</p>
                    <small style="bottom: 0px; top: auto; display: inline-block; position: initial">{{$item->created_at->diffForHumans()}}</small>
                </div> 
            </div>
        </a>
      </div>
    @empty
        <h3>Buku tidak ditemukan !</h3>
    @endforelse
  </div>  
</div>
@endsection