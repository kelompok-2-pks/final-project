@extends('layouts.main')

@section('judul')
<h1 class="text-center" style="color: aliceblue"></h1>
@endsection
    
@section('content')
<div class="row">
    <h4 class="container-fluid mt-3" id="olahraga"><b>Perlengkapan Olahraga</b></h4>
    
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="{{asset('logo/TokoAnakNegeri.png')}}" alt="First slide">
            </div>
        @foreach ($buku as $item)
            <div class="carousel-item ">
                <div class="card-wrapper">
                    <div class="card">
                        <div class="image-wrapper">
                          <img src="{{ asset('img/'. $item->thumbnail) }}" alt="Card image cap">
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">{{$item->judulbuku}}</h5>
                            <p class="card-text">{{Str::limit($item->sinopsis, 50, $end='...')}}</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                      </div>
                </div>
            </div>
            <div class="carousel-item ">
                <div class="card-wrapper">
                    <div class="card">
                        <div class="image-wrapper">
                          <img src="{{ asset('img/'. $item->thumbnail) }}" alt="Card image cap">
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">{{$item->judulbuku}}</h5>
                            <p class="card-text">{{Str::limit($item->sinopsis, 50, $end='...')}}</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                      </div>
                </div>
            </div>
        @endforeach
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    
@endsection