@extends('layouts.main')

@section('judul')
<h1 class="text-center" style="color: aliceblue">Edit Buku</h1>
@endsection
    

@section('content')
<form action="/buku/{{$buku->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    
    <div class="form-group">
      <label>Judul Buku</label>
      <input type="text" class="form-control text-light" name="judulbuku" value="{{$buku->judulbuku}}">
    </div>
    @error('judulbuku')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label>Sinopsis</label>
      <textarea  class="form-control text-light" rows="10" name="sinopsis">{{$buku->sinopsis}}</textarea>
    </div>
    @error('sinopsis')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label>Penulis</label>
      <input type="text" class="form-control text-light" name="penulis" value="{{$buku->penulis}}">
    </div>
    @error('penulis')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label>Penerbit</label>
      <input type="text" class="form-control text-light" name="penerbit" value="{{$buku->penerbit}}">
    </div>
    @error('penerbit')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label>Tahun Terbit</label>
      <input type="number" class="form-control text-light" name="tahun" value="{{$buku->tahun}}">
    </div>
    @error('tahun')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label>Jumlah Halaman</label>
      <input type="number" class="form-control text-light" name="halaman" value="{{$buku->halaman}}">
    </div>
    @error('halaman')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
        <label>Genre</label>
        <select name="genre_id" class="js-example-basic-single " style="width: 100%">
          <option value="">---Pilih Genre---</option>
          @foreach ($genre as $item)
            @if ($item->id === $buku->genre_id)
                <option value="{{$item->id}}" selected>{{$item->nama}}</option>
            @else
            <option value="{{$item->id}}">{{$item->nama}}</option>
            @endif
          @endforeach
        </select>
    </div>
    @error('genre_id')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label>Thumbnail</label>
      <input type="file" class="form-control text-light" name="thumbnail" value="{{$buku->thumbnail}}" > 
    </div>
    @error('thumbnail')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection