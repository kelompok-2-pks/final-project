@extends('layouts.main')

@section('judul')
    <h1 class="text-center" style="color: aliceblue">Data Profil Anda</h1>
@endsection

@section('content')
<section class="mt-4">
    <hr class="bg-light">
    <div class="row">
        <div class="col-2">
            <h4>Nama</h4>
            <h4>Email</h4>
            <h4>username</h4>
            <h4>Umur</h4>
            <h4>Bio</h4>
            <h4>Alamat</h4>
        </div>
        <div class="col-10">
            <h4>: {{$profil->user->name}}</h4>
            <h4>: {{$profil->user->email}}</h4>
            <h4>: {{$profil->username}}</h4>
            <h4>: {{$profil->umur}}</h4>
            <h4>: {{$profil->bio}}</h4>
            <h4>: {{$profil->alamat}}</h4>
        </div>
    </div>
</section>
    <hr class="bg-light">

    <a href="/profil/{{$profil->id}}" class="btn btn-primary"> Ubah Profile</a>
@endsection