@extends('layouts.main')

@section('judul')
    <h1 class="text-center" style="color: aliceblue">Ubah Profil</h1>
@endsection

@section('content')
<form action="/profil/{{$profil->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control bg-dark" value="{{$profil->user->name}}" disabled>
    </div>
    
    <div class="form-group">
        <label>Email</label>
        <input type="text" class="form-control bg-dark" value="{{$profil->user->email}}" disabled>
    </div>  
    
    <div class="form-group">
        <label>username</label>
        <input type="text" class="form-control text-light" name="username" value="{{$profil->username}}">
    </div>  
    
    <div class="form-group">
      <label>Umur</label>
      <input type="number" class="form-control text-light" value="{{$profil->umur}}" name="umur">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label>alamat</label>
      <textarea  class="form-control text-light" name="alamat">{{$profil->alamat}}</textarea>
    </div>
    @error('alamat')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label>Bio</label>
      <textarea  class="form-control text-light" name="bio">{{$profil->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection