@extends('layouts.main')

@section('judul')
<h1 class="text-center" style="color: aliceblue">Daftar Genre</h1>
@endsection

@section('content')
<a href="/genre/create" class="btn btn-secondary mb-3">Tambah Genre</a>
  
<table class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col">No</th>
      <th scope="col">Name</th>
      <th scope="col" class="text-center">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($genre as $key => $item)
        <tr class="text-light">
          <td>{{$key + 1}}</td>
          <td>{{$item -> nama}}</td>
          <td class="text-center"> 
            <a href="/genre/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
          </td>
        </tr>
    @empty
        <h1>Data Tidak Ada!</h1>
    @endforelse
  </tbody>
</table>

    
@endsection