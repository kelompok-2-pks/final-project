@extends('layouts.main')

@section('judul')
    <h1 class="text-center" style="color: aliceblue">Tambah Genre</h1>
@endsection

@section('content')
<form action="/genre" method="POST" class="mt-5">
    @csrf
    <div class="form-group ">
      <label>Nama Genre Buku</label>
      <input type="text" class="form-control bg-light mt-2" name="nama">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection