@extends('layouts.main')

@section('judul')
<h1 class="text-center" style="color: aliceblue">Tentang Website MyBook</h1>
@endsection

@section('content')
<div class="container-fluid d-flex justify-content-center my-5" >
    <div class="col-md-8 text-center text-dark" style="background-color: #f5eedc; border-radius: 20px">
        <p style="font-size: 1.3em"><b>Mybook</b> adalah sebuah website review buku yang memberikan berbagai informasi tentang sebuah buku.</p>
        <p  style="font-size: 1.3em">Website ini menggunakan template <a href="https://themewagon.com/themes/corona-free-responsive-bootstrap-4-admin-dashboard-template/">CORONA</a>.</p>
        <p style="font-size: 1.1em">Website ini dibuat sebagai tugas akhir Bootcamp <br> <a href="https://www.pksdigitalschool.id/">PKS digital school</a> Batch 3.</p>
        <p style="font-size: 1.1em">Website ini dibuat oleh :</p>
        <p style="font-size: 1.1em">1. Muhammad Naufal Jatiageng </p>
        <p style="font-size: 1.1em">2. Tia Mutiara</p>
        <p style="font-size: 1.1em">3. Wildan Putra Delianda</p>
    </div>
</div>
@endsection