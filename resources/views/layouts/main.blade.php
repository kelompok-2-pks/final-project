<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>MyBook</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{asset('/admin/vendors/mdi/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/vendors/css/vendor.bundle.base.css')}}">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="{{asset('/admin/vendors/jvectormap/jquery-jvectormap.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/vendors/flag-icon-css/css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/vendors/owl-carousel-2/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="/admin/vendors/owl-carousel-2/owl.theme.default.min.css')}}">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="{{asset('/admin/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('/css/style.css')}}">
    @stack('style')
    <style>
      @import url('https://fonts.googleapis.com/css2?family=Noto+Serif&family=Source+Sans+Pro&display=swap');
    </style>
    <!-- End layout styles -->
    <link rel="shortcut icon" href="{{asset('/admin/images/favicon.png')}}" />
  </head>
  <body>
    <div class="container-scroller">
    @include('sweetalert::alert')

      <!-- partial:partials/_sidebar.html -->
      @include('layouts.partials.sidebar')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_navbar.html -->
        @include('layouts.partials.navbar')
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper" id="hasilcari">
            @yield('judul')
            @yield('content')
          </div>
          <!-- content-wrapper ends -->
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="{{asset('/admin/vendors/js/vendor.bundle.base.js')}}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="{{asset('/admin/vendors/chart.js/Chart.min.js')}}"></script>
    <script src="{{asset('/admin/vendors/progressbar.js/progressbar.min.js')}}"></script>
    <script src="{{asset('/admin/vendors/jvectormap/jquery-jvectormap.min.js')}}"></script>
    <script src="{{asset('/admin/vendors/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
    <script src="{{asset('/admin/vendors/owl-carousel-2/owl.carousel.min.js')}}"></script>
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{asset('/admin/js/off-canvas.js')}}"></script>
    <script src="{{asset('/admin/js/hoverable-collapse.js')}}"></script>
    <script src="{{asset('/admin/js/misc.js')}}"></script>
    <script src="{{asset('/admin/js/settings.js')}}"></script>
    <script src="{{asset('/admin/js/todolist.js')}}"></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src="{{asset('/admin/js/dashboard.js')}}"></script>
    <script src="{{asset('/js/search.js')}}"></script>
    @stack('scripts')
    <!-- End custom js for this page -->
  </body>
</html>