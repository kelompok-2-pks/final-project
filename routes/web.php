<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//CRUD Buku
    Route::get('/', 'BukuController@index'); 
    Route::get('/cari', 'BukuController@cari'); 
    Route::get('/create', 'BukuController@create');
    Route::post('/', 'BukuController@store');
    Route::get('/buku/{id}','BukuController@show'); 
    Route::get('/buku/{id}/edit', 'BukuController@edit'); 
    Route::put('/buku/{id}', 'BukuController@update');
    Route::delete('/buku/{id}', 'BukuController@destroy');


Route::resource('genre', 'GenreController');
Route::resource('profil', 'ProfilController');
Route::resource('komentar', 'KomentarController');
Route::get('/about', function(){
    return view('about');
});
Auth::routes();
