<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table = 'bukus';
    
    protected $guarded = ['id'];

    public function genre(){
        return $this->belongsTo('App\Genre');
    }

    public function komentar(){
        return $this->hasMany('App\Komentar');
    }
}
 