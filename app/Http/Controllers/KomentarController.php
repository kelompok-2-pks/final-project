<?php

namespace App\Http\Controllers;

use App\Komentar;
use Illuminate\Http\Request;
use illuminate\Support\Facades\Auth;


class KomentarController extends Controller
{
    
    public function store(Request $request)
    {
        $request->validate([
            'isi' => 'required',
        ]);

        $komentar = new komentar;

        $komentar->isi = $request->isi;
        $komentar->user_id = Auth::id();
        $komentar->buku_id = $request->buku_id;

        $komentar->save();

        return redirect()->back();
    }
}
