<?php

namespace App\Http\Controllers;

use App\Buku;
use App\Genre;
use App\Komentar;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use File;


class BukuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show','cari']);
    }
    
    
    public function index()
    {
        $buku = Buku::all();

        return view('buku.index', compact('buku'));
    }

    
    public function create()
    {   
        $genre = Genre::all();
        return view('buku.create', compact('genre'));
    }

    
    public function store(Request $request)
    {
        $request->validate([
            'judulbuku' => 'required',
            'sinopsis' => 'required',
            'thumbnail' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'penulis' => 'required',
            'tahun' => 'required',
            'halaman' => 'required',
            'penerbit' => 'required',
            'genre_id' => 'required',
        ]);

        $thumbnailName = time().'.'.$request->thumbnail->extension();
        $request->thumbnail->move(public_path('img'), $thumbnailName);

        $buku = new Buku;
 
        $buku->judulbuku = $request->judulbuku;
        $buku->sinopsis = $request->sinopsis;
        $buku->thumbnail = $thumbnailName;
        $buku->penulis = $request->penulis;
        $buku->tahun = $request->tahun;
        $buku->halaman = $request->halaman;
        $buku->penerbit = $request->penerbit;
        $buku->genre_id = $request->genre_id;
 
        $buku->save();

        Alert::success('Berhasil !', 'Anda berhasil menambahkan buku.');


        return redirect('/');
    }

    
    public function show($id)
    {
        $buku = Buku::where('id', $id)->first();
        return view('buku.show', compact('buku'));
    }

    
    public function edit($id)
    {
        $buku = Buku::find($id);
        $genre = Genre::all();
        return view('buku.edit', compact('buku', 'genre'));
    }

    
    public function update(Request $request, $id)
    {
        $request->validate([
            'judulbuku' => 'required',
            'sinopsis' => 'required',
            'thumbnail' => 'image|mimes:jpeg,png,jpg|max:2048',
            'penulis' => 'required',
            'tahun' => 'required',
            'halaman' => 'required',
            'penerbit' => 'required',
            'genre_id' => 'required',
        ]);

        $buku = Buku::find($id);

        if ($request->has('thumbnail')) {
           $thumbnailName = time().'.'.$request->thumbnail->extension();
           $request->thumbnail->move(public_path('img'), $thumbnailName);
    
           $buku->judulbuku = $request->judulbuku;
           $buku->sinopsis = $request->sinopsis;
           $buku->thumbnail = $thumbnailName;
           $buku->penulis = $request->penulis;
           $buku->tahun = $request->tahun;
           $buku->halaman = $request->halaman;
           $buku->penerbit = $request->penerbit;
           $buku->genre_id = $request->genre_id;
        } else {
            $buku->judulbuku = $request->judulbuku;
            $buku->sinopsis = $request->sinopsis;
            $buku->penulis = $request->penulis;
            $buku->tahun = $request->tahun;
            $buku->halaman = $request->halaman;
            $buku->penerbit = $request->penerbit;
            $buku->genre_id = $request->genre_id;
        }
        

        $buku->update();

        Alert::success('Berhasil !', 'Anda berhasil mengubah buku.');


        return redirect('/');
    }

    
    public function destroy($id)
    {
        $buku = Buku::find($id);
        $komen = Komentar::where('buku_id', $id);
        $path = "img/";
        File::delete($path. $buku->thumbnail);
        
        $komen->delete();
        $buku->delete();

        Alert::success('Berhasil !', 'Anda berhasil menghapus film '. ($buku->judulbuku ));

        return redirect('/');
    }

    public function cari(Request $request){
        //menangakap data pencarian
        $cari = $request->cari;

        //mengambil data buku sesuai request pencarian
        $buku = Buku::where('judulbuku','like',"%".$cari."%")->paginate();

        return view('buku.index', compact('buku'));

    }
}
