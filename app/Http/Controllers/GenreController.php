<?php

namespace App\Http\Controllers;

use App\Genre;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class GenreController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }

    
    public function index()
    {
        $genre = Genre::all();
        return view('genre.index', compact('genre'));
    }

    
    public function create()
    {
        return view('genre.create');
    }

    
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
        ]);

        $genre = new Genre;

        $genre->nama = $request->nama;
        
        $genre->save();

        Alert::success('Berhasil !', 'Anda berhasil menambahkan genre.');

        return redirect('/genre');
    }

    
    public function show($id)
    {
        $genre = Genre::find($id);
        return view('genre.show', compact('genre'));
    }
}
