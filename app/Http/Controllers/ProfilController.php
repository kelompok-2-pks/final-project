<?php

namespace App\Http\Controllers;

use App\Profil;
use Illuminate\Http\Request;
use illuminate\Support\Facades\Auth;


class ProfilController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $profil = Profil::where('user_id', Auth::id())->first();
        
        return view('profil.index', compact('profil'));
    }

    
    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        //
    }

    
    public function show($id)
    {
        $profil = Profil::find($id);
        return view('profil.update', compact('profil'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'username' =>'required',
            'umur' => 'required',
            'alamat' => 'required',
            'bio' => 'required',
        ]);

        $profil = Profil::find($id);

        $profil->umur = $request['umur'];
        $profil->bio = $request['bio'];
        $profil->alamat = $request['alamat'];
        $profil->username = $request['username'];

        $profil->save();

        return redirect('/profil');
    }
}
