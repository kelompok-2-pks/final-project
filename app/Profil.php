<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profil extends Model
{
    protected $fillable = ['username', 'umur', 'alamat', 'bio', 'user_id'];

    protected function user(){
        return $this->belongsTo('App\User');
    }
}
