<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class komentar extends Model
{
    protected $fillable = ['user_id', 'buku_id', 'isi'];

    protected function buku(){
        return $this->belongsTo('App\Buku');
    }

    protected function user(){
        return $this->belongsTo('App\User');
    }
}
